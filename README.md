# k8s_0_0

  

1. Офиц мануал говорит что ансибл имеyно для кубспрея сложно настраивается поэтому они советуют эту штуку:
  
*A simple way to ensure you get all the correct version of Ansible is to use the pre-built docker image from Quay. You will then need to use bind mounts to access the inventory and SSH key in the container.*

Пошел этим путем, то есть развернул контейнер с ансиблом.

 
2. Поднял 2 машины на AWS.
Обязательное требование было минимум 2 ГБ РАМ, иначе ансибл плейбук не работал.

На машинах использован образ AMI Ubuntu 20. Проблем с Убунту 20 обнаружены не было, никаких сложностей =)

Выкладываю в гит файлы, который модифицировал/использовал.


Команда для запуска
*ansible-playbook -i ./inventory/sample/inventory.ini  -u ubuntu  --become cluster.yml*

3. Когда все поставилось зашел на мастер ноду и сделал 2 команды

***chmod +r /etc/kubernetes/admin.conf***

***export KUBECONFIG=/etc/kubernetes/admin.conf***

 После этого у меня стартовало в таком режиме:

***kubectl get nodes***

*NAME STATUS ROLES AGE VERSION*

*ec2-master Ready control-plane 37m v1.25.6*

*ec2-worker Ready <none> 33m v1.25.6kubectl get pods -n cert-manager*

***kubectl get pod -n ingress-nginx***

*NAME READY STATUS RESTARTS AGE*

*ingress-nginx-controller-ddcx9 1/1 Running 0 4m34s*

**kubectl get pods -n cert-manager**

*NAME READY STATUS RESTARTS AGE*

*cert-manager-7cf64df488-fvt7k 1/1 Running 0 6m7s*

*cert-manager-cainjector-68b9bf79b5-5wx7f 1/1 Running 0 6m7s*

*cert-manager-webhook-67c9fc455f-t4bzq 1/1 Running 0 6m7s*

  
  
  

4. Машинки в AWS платные, так что погасил. Если стартовать заново , нужно будет поменять только IP Public и Private сети в инвентори.